<?php

namespace Sso\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Token
 *
 * @ORM\Table(name="token", indexes={@ORM\Index(name="IDX_5F37A13B2024BA54", columns={"original_signin_id"})})
 * @ORM\Entity(repositoryClass="Sso\SecurityBundle\Repository\TokenRepository")
 */
class Token
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="token_temp",type="string", length=120, nullable=false)
     */
    private $tokenTemp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_at", type="datetime", nullable=false)
     */
    private $expireAt;
    /**
     * @var \Signin
     *
     * @ORM\ManyToOne(targetEntity="Signin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="original_signin_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $originalSignin;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tokenTemp
     *
     * @param string $tokenTemp
     * @return Token
     */
    public function setTokenTemp($tokenTemp)
    {
        $this->tokenTemp = $tokenTemp;

        return $this;
    }

    /**
     * Get tokenTemp
     *
     * @return string 
     */
    public function getTokenTemp()
    {
        return $this->tokenTemp;
    }

    /**
     * Set expireAt
     *
     * @param \DateTime $expireAt
     * @return Token
     */
    public function setExpireAt($expireAt)
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    /**
     * Get expireAt
     *
     * @return \DateTime 
     */
    public function getExpireAt()
    {
        return $this->expireAt;
    }

    /**
     * Set originalSignin
     *
     * @param \Sso\SecurityBundle\Entity\Signin $originalSignin
     * @return Token
     */
    public function setOriginalSignin(\Sso\SecurityBundle\Entity\Signin $originalSignin = null)
    {
        $this->originalSignin = $originalSignin;

        return $this;
    }

    /**
     * Get originalSignin
     *
     * @return \Sso\SecurityBundle\Entity\Signin 
     */
    public function getOriginalSignin()
    {
        return $this->originalSignin;
    }
}
