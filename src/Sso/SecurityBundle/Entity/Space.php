<?php

namespace Sso\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Space
 *
 * @ORM\Table(name="space")
 * @ORM\Entity
 */
class Space
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url_logout", type="string", length=255, nullable=true)
     */
    private $urlLogout;



    /**
     * Set name
     *
     * @param string $name
     * @return Space
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set urlLogout
     *
     * @param string $urlLogout
     * @return Space
     */
    public function setUrlLogout($urlLogout)
    {
        $this->urlLogout = $urlLogout;

        return $this;
    }

    /**
     * Get urlLogout
     *
     * @return string 
     */
    public function getUrlLogout()
    {
        return $this->urlLogout;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
