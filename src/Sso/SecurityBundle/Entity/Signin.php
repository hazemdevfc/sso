<?php

namespace Sso\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Signin
 *
 * @ORM\Table(name="signin", indexes={@ORM\Index(name="IDX_52DB7ED1A76ED395", columns={"user_id"}), @ORM\Index(name="IDX_52DB7ED1B6885C6C", columns={"space_id"}), @ORM\Index(name="user_id", columns={"user_id", "space_id"})})
 * @ORM\Entity(repositoryClass="Sso\SecurityBundle\Repository\SigninRepository")
 */
class Signin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=120, nullable=false)
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_at", type="datetime", nullable=false)
     */
    private $expireAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="expired", type="boolean", nullable=true)
     */
    private $expired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disconnected", type="boolean", nullable=true)
     */
    private $disconnected;

    /**
     * @var \Space
     *
     * @ORM\ManyToOne(targetEntity="Space")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="space_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $space;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Signin
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set expireAt
     *
     * @param \DateTime $expireAt
     * @return Signin
     */
    public function setExpireAt($expireAt)
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    /**
     * Get expireAt
     *
     * @return \DateTime 
     */
    public function getExpireAt()
    {
        return $this->expireAt;
    }

    /**
     * Set expired
     *
     * @param boolean $expired
     * @return Signin
     */
    public function setExpired($expired)
    {
        $this->expired = $expired;

        return $this;
    }

    /**
     * Get expired
     *
     * @return boolean 
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * Set disconnected
     *
     * @param boolean $disconnected
     * @return Signin
     */
    public function setDisconnected($disconnected)
    {
        $this->disconnected = $disconnected;

        return $this;
    }

    /**
     * Get disconnected
     *
     * @return boolean 
     */
    public function getDisconnected()
    {
        return $this->disconnected;
    }

    /**
     * Set space
     *
     * @param \Sso\SecurityBundle\Entity\Space $space
     * @return Signin
     */
    public function setSpace(\Sso\SecurityBundle\Entity\Space $space = null)
    {
        $this->space = $space;

        return $this;
    }

    /**
     * Get space
     *
     * @return \Sso\SecurityBundle\Entity\Space 
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * Set user
     *
     * @param \Sso\SecurityBundle\Entity\User $user
     * @return Signin
     */
    public function setUser(\Sso\SecurityBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sso\SecurityBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
