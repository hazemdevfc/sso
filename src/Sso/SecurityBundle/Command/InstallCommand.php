<?php
namespace Sso\SecurityBundle\Command;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\DriverManager;

class InstallCommand extends \Doctrine\Bundle\DoctrineBundle\Command\DoctrineCommand
{
    protected function configure()
    {
        $this
            ->setName('sso:install:demo')
            ->setDescription('Installation of demo data');
    }

    protected function execute(InputInterface $oInput, OutputInterface $oOutput)
    {

        $connection = $this->getDoctrineConnection(null);
        $params = $connection->getParams();
        $sDbSso = isset($params['path']) ? $params['path'] : $params['dbname'];
        unset($params['dbname']);

        /** @var $oDbalConnection \Doctrine\DBAL\Connection */
        $oDbalConnection = DriverManager::getConnection($params);


        /*
       * List of query so that everything works
       */
        $aQueries = array(
            array("message" => "Insert the first example of space",
                "sql" => "INSERT INTO `" . $sDbSso . "`.`space` (`id`, `name`, `url_logout`) VALUES
                     (NULL, 'Administration A', 'http://admina.santevet.com/logout');",
                "tag" => "Espace A"
            ),
            array("message" => "Insert other examples of space",
                "sql" => "INSERT INTO `" . $sDbSso . "`.`space` (`id`, `name`, `url_logout`) VALUES
                      (NULL, 'Administration B', 'http://adminb.santevet.com/logout'),
                      (NULL, 'Administration C', 'http://adminc.santevet.com/logout'),
                      (NULL, 'Administration D', 'http://admind.santevet.com/logout')
                     ;",
                "tag" => ""
            ),
            array("message" => "Select id of user santevet if exist",
                "sql" => "SELECT id FROM `" . $sDbSso . "`.`user` WHERE login like 'santevet';",
                "tag" => "Select User"
            ),
            array("message" => "Insert example of user",
                "sql" => "INSERT INTO `" . $sDbSso . "`.`user` (`id`, `login`, `password`, `last_login`, `created_at`, `updated_at`) VALUES
                (NULL, 'santevet', SHA1('santevet'), '2016-06-27 09:00:00', '2016-06-27 08:00:00', '2016-06-27 08:00:00');",
                "tag" => "Insert User"
            ),
            array("message" => "Insert example of signin",
                "sql" => "INSERT INTO `" . $sDbSso . "`.`signin` (`id`, `user_id`, `space_id`, `token`, `expire_at`, `expired`, `disconnected`) VALUES
                (NULL, '%s', '%s', '8b0d9c93a3545d0d99f6486208dd3659ec802ee1', '2017-01-01 10:50:00', '0', '0'),
                (NULL, '%s', '%s', '5ed49c93a3545d0d99f6486208dd3659e5jljdee', '2016-06-01 10:50:00', '1', '0')
                ;",
                "tag" => "Signin"
            ),

        );
        foreach ($aQueries as $aQuery) {
            try {
                if ($aQuery['tag'] == "Select User") {
                    $oStatement = $oDbalConnection->executeQuery($aQuery['sql']);
                    if ($oStatement->rowCount() > 0) {
                        $aRow = $oStatement->fetch();
                        $sUserId = $aRow['id'];
                        $oOutput->writeln($aQuery['message'] . ": OK-".$sUserId);
                    } else {
                        $sUserId = '';
                    }
                } elseif ($aQuery['tag'] == "Insert User") {
                    if (!isset($sUserId) || ($sUserId == '')) {
                        $oStatement = $oDbalConnection->executeQuery($aQuery['sql']);
                        $oOutput->writeln($aQuery['message'] . ": OK");
                    }
                } elseif (($aQuery['tag'] == "Signin")) {

                    if (isset($sUserId) && isset($sSpaceAId)) {
                        $sQuery = sprintf($aQuery['sql'], $sUserId, $sSpaceAId,$sUserId, $sSpaceAId);
                        $oStatement = $oDbalConnection->executeQuery($sQuery);
                        $oOutput->writeln($aQuery['message'] . ": OK");
                    }
                } else {
                    $oStatement = $oDbalConnection->executeQuery($aQuery['sql']);
                    $oOutput->writeln($aQuery['message'] . ": OK");
                }

                if ($aQuery['tag'] == "Espace A") {
                    $sSpaceAId = $oDbalConnection->lastInsertId();
                }

            } catch (\Exception $e) {
                $oOutput->writeln(sprintf('<error>Could not execute : %s </error>', $aQuery['message']));
                $oOutput->writeln(sprintf('<error>%s</error>', $e->getMessage()));
                $oDbalConnection->close();
                exit;
            }
        }

        $oDbalConnection->close();
    }
}