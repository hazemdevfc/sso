<?php
/**
 * SsoSecurityBundle SigninRepository
 */
namespace Sso\SecurityBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * TokenRepository
 */
class TokenRepository extends EntityRepository
{

    /**
     * Used to chek if exist a valid token for the signup
     * @param $oSignin
     * @return array
     */
    public function getValidTokenBySigninLogin( $oSignin )
    {
        $oNow=new \DateTime();
        $sFormattedNow = $oNow->format('Y-m-d 00:00:00');
        /*Initial query*/
        $query = $this->getEntityManager( )->createQueryBuilder( )
            ->select( 't' )
            ->from( 'SsoSecurityBundle:Token', 't'  )
            ->join('t.originalSignin', 's')
            ->andWhere(' t.expireAt > \''.$sFormattedNow.'\'')
            ->andWhere( 's.id =:signin' )
            ->setParameter("signin",$oSignin->getId());

        /*Execute*/
        return $query->getQuery()->getResult();
    }

    /**
     * @param $sTokenTemp
     * @return array
     */
    public function getValidTokenByTokenTempLogin( $sTokenTemp, $sLogin )
    {
        $oNow=new \DateTime();
        $sFormattedNow = $oNow->format('Y-m-d 00:00:00');
        /*Initial query*/
        $query = $this->getEntityManager( )->createQueryBuilder( )
            ->select( 't' )
            ->from( 'SsoSecurityBundle:Token', 't'  )
            ->join('t.originalSignin', 's')
            ->join('s.user', 'u')
            ->andWhere(' t.expireAt > \''.$sFormattedNow.'\'')
            ->andWhere( 't.tokenTemp like \''.$sTokenTemp.'\'' )
            ->andWhere( 'u.login like \''.$sLogin.'\'' );

        /*Execute*/
        return $query->getQuery()->getResult();
    }
}
