<?php
/**
 * SsoSecurityBundle SigninRepository
 */
namespace Sso\SecurityBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * SigninRepository
 */
class SigninRepository extends EntityRepository
{

    /**
     * Used to chek if exist a signin with token and login
     * @param $sToken
     * @param $sLogin
     */
    public function getSigninByTokenLogin( $sTokenSession, $sLogin )
    {
        /*Initial query*/
        $query = $this->getEntityManager( )->createQueryBuilder( )
            ->select( 's' )
            ->from( 'SsoSecurityBundle:Signin', 's'  )
            ->join('s.user', 'u')
            ->andWhere( 's.token like \''.$sTokenSession.'\'' )
            ->andWhere( 'u.login like \''.$sLogin.'\'' );
        /*Execute*/
        return $query->getQuery()->getResult();
    }
}
