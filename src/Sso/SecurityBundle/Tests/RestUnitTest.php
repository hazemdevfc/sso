<?php

namespace Sso\SecurityBundle\Tests;

use Sso\SecurityBundle\Controller\RestController;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RestUnitTest
 * To handle unit tests only: run this command: phpunit src/Sso/SecurityBundle/Tests/RestUnitTest
 * To handle all tests: run this command: phpunit src/Sso/ScurityBundle/Tests/
 * @package Sso\SecurityBundle\Tests
 */
class RestUnitTest extends KernelTestCase
{
    private $container;

    public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    public function testGenerateTokenTemp()
    {
        /*Case 1: session token valid*/
        $aResponse = $this->container->get('sso_security.sso_rest_core')->generateTokenTemp("8b0d9c93a3545d0d99f6486208dd3659ec802ee1", "santevet");
        $this->assertArraySubset(['status' => 200], $aResponse);

        /*Case 2: session token not found*/
        $aResponse = $this->container->get('sso_security.sso_rest_core')->generateTokenTemp("test_token", "santevet");
        $this->assertArraySubset(['data' => ['error' => "No session found with given parameters!"]], $aResponse);

        /*Case 3: expired session*/
        $aResponse = $this->container->get('sso_security.sso_rest_core')->generateTokenTemp("5ed49c93a3545d0d99f6486208dd3659e5jljdee", "santevet");
        $this->assertArraySubset(['data' => ['error' => "Expired Session!"]], $aResponse);

        /*Case 4: missed parameter*/
        $aResponse = $this->container->get('sso_security.sso_rest_core')->generateTokenTemp("santevet");
        $this->assertArraySubset(['data' => ['error' => "Missed parameter (token_session,login)!"]], $aResponse, false, "test123");
    }

    public function testLoginToken()
    {
        /*Get temp token to be used in login action*/
        $aResponse = $this->container->get('sso_security.sso_rest_core')->generateTokenTemp("8b0d9c93a3545d0d99f6486208dd3659ec802ee1", "santevet");
        $sTokenTemp = $aResponse['data']['token_temp'];
        $sExpiration = $aResponse['data']['expiration'];

        /*Case 1: session token valid*/
        $aResponse = $this->container->get('sso_security.sso_rest_core')->loginToken("santevet",$sTokenTemp);
        $this->assertArraySubset(['status' => 200], $aResponse);

        /*Case 2: token temp not found*/
        $aResponse = $this->container->get('sso_security.sso_rest_core')->loginToken("santevet","test_token_temp");
        $this->assertArraySubset(["data"=>['error' => "No token temp found with given parameters!"]], $aResponse);

        /*Case 3: missed parameter*/
        $aResponse = $this->container->get('sso_security.sso_rest_core')->loginToken("santevet","");
        $this->assertArraySubset(["data"=>['error' => "Missed parameter (token_temp,login)!"]], $aResponse);

    }


}
