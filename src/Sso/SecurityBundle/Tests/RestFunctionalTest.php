<?php

namespace Sso\SecurityBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class RestFunctionalTest
 * To handle functional tests only: run this command: phpunit src/Sso/ScurityBundle/Tests/RestFunctionalTest
 * To handle all tests: run this command: phpunit src/Sso/ScurityBundle/Tests/
 * @package Sso\SecurityBundle\Tests
 */
class RestFunctionalTest extends WebTestCase
{
    public function setUp()
    {
        $this->client = static::createClient();
    }


    public function testGenerateTempTokenAction()
    {
        $client = static::createClient();

        /*Case 1: session token valid*/
        $aParameters = array("token_session" => "8b0d9c93a3545d0d99f6486208dd3659ec802ee1", "login" => "santevet");
        $crawler = $client->request('PUT', '/generate/token/temp.json', $aParameters);
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertJsonResponse($response, 200);
        $result = json_decode($content, true);

        /*Case 2: session token not found*/
        $aParameters = array("token_session" => "test_token", "login" => "santevet");
        $crawler = $client->request('PUT', '/generate/token/temp.json', $aParameters);
        $response = $client->getResponse();
        $content = $response->getContent();
        $result = json_decode($content, true);
        $this->assertEquals("No session found with given parameters!", $result['error']);

        /*Case 3: expired session*/
        $aParameters = array("token_session" => "5ed49c93a3545d0d99f6486208dd3659e5jljdee", "login" => "santevet");
        $crawler = $client->request('PUT', '/generate/token/temp.json', $aParameters);
        $response = $client->getResponse();
        $content = $response->getContent();
        $result = json_decode($content, true);
        $this->assertEquals("Expired Session!", $result['error']);

        /*Case 4: missed parameter*/
        $aParameters = array("login" => "santevet");
        $crawler = $client->request('PUT', '/generate/token/temp.json', $aParameters);
        $response = $client->getResponse();
        $content = $response->getContent();
        $result = json_decode($content, true);
        $this->assertEquals("Missed parameter (token_session,login)!", $result['error']);

    }

    public function testloginTokenAction()
    {
        $client = static::createClient();

        /*Get temp token to be used in login action*/
        $aParameters = array("token_session" => "8b0d9c93a3545d0d99f6486208dd3659ec802ee1", "login" => "santevet");
        $crawler = $client->request('PUT', '/generate/token/temp.json', $aParameters);
        $response = $client->getResponse();
        $content = $response->getContent();
        $result = json_decode($content, true);
        $sTokenTemp = $result['token_temp'];
        $sExpiration = $result['expiration'];

        /*Case 1: session token valid*/
        $aParameters = array("login" => "santevet", "token_temp" => $sTokenTemp);
        $crawler = $client->request('POST', '/login/token.json', $aParameters);
        $response = $client->getResponse();
        $content = $response->getContent();
        $this->assertJsonResponse($response, 200);
        $result = json_decode($content, true);

        /*Case 2: token temp not found*/
        $aParameters = array("login" => "santevet", "token_temp" => "test_token_temp");
        $crawler = $client->request('POST', '/login/token.json', $aParameters);
        $response = $client->getResponse();
        $content = $response->getContent();
        $result = json_decode($content, true);
        $this->assertEquals("No token temp found with given parameters!", $result['error']);

        /*Case 3: missed parameter*/
        $aParameters = array("login" => "santevet");
        $crawler = $client->request('POST', '/login/token.json', $aParameters);
        $response = $client->getResponse();
        $content = $response->getContent();
        $result = json_decode($content, true);
        $this->assertEquals("Missed parameter (token_temp,login)!", $result['error']);



    }

    /**
     * Check the type of response is json and having success status
     * @param $response
     * @param int $statusCode
     */
    protected function assertJsonResponse($response, $statusCode = 200)
    {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(), $response->getContent()
        );
        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            $response->headers
        );
    }
}
